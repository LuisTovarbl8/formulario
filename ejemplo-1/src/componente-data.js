import { LitElement } from "lit-element";
export class ComponenteData extends LitElement{

    static get properties(){
        return{
            url:{type: String},
            method:{type:String}
        }
    }

    firstUpdated(){
        this.getData();
    }

    _sendData(data){
        this.dispatchEvent(new CustomEvent('ApiData',{
            detail:{data}, bubbles: true, composed:true
        }));
    }
    getData(){
        console.log(this.url);
        fetch(this.url,{method: this.method})
        .then((response)=>{
            if(response.ok) return response.json();
            return Promise.reject(response);
        })
        .then((data)=>{this._sendData(data);})
        .catch((error)=>{console.warn('Algo sucedio!!!', error);})
    }

}
customElements.define('componente-data', ComponenteData);