import { html, css, LitElement } from 'lit-element';
import './componente-data';

export class Ejemplo1 extends LitElement {
    static get styles() {
		return css`
        table{
            background: white;
            color: black;
            width: 50%;
            font-size: 18px;
            border: 1px solid gray;
            display:table;
            border-collapse: collapse;
            width: 80%;
        }
        th, td{
            width: 10%;
            text-align: left;
            vertical-align: top;
        }
        table tbody tr:nth-child(odd) {
            background: white;
        }
        table tbody tr:nth-child(even) {
            background: grey;
        }
        `;
    }
    static get properties(){
        return{
            forma:{type: Array},
            fil:{type: Array},
            algo:{type: String}
        };
    }
    constructor(){
        super();
        this.forma=[];
        this.addEventListener('ApiData',(e)=>{
        this._dataFormat(e.detail.data);
        //console.log(e.detail.data);
        })
    }
    // filtro(ev){
    //     this.fil= ev.detail.data;
    //     console.log(this.fil);
    // }

    _dataFormat(data){
        let characters=[];

        data["data"].forEach((character)=>{
            characters.push({
                id: character.id,
                employee_name: character.employee_name,
                employee_salary: character.employee_salary
            })
        })
        this.forma= characters;
    }
    // _buscar(){
    //     fetch('http://dummy.restapiexample.com/api/v1/employees'+this.inputValue)
    //     .then(res=> res.json())
    //     .then(data=>{
    //         this._dataFormat(data);
    //     })
    //     .catch(function(e){
    //         alert('Error al cargar'+e);
    //     })
    // }

    render(){
        return html`
            <componente-data url="http://dummy.restapiexample.com/api/v1/employees" method="GET"></componente-data>
            <div class="superior">
                <p>View: SamplePage</p>
            </div>
            <form class="contenedor">
                <p>filter:</p><form action="" .value=""><input  type="text" placeholder="Ingresa el nombre"></form> 
                <button>Buscar</button>
                    <input type="reset" value="borrar">
            </form>
            ${this.dateTemplate}
            <div class="bot">
                        <input type="button" value="Ok">
                        <input type="button" value="Cancel" >
            </div>
        `;
    }
    // employee_name(event){
    //     this.algo= event.target.value;
    // }

    get dateTemplate(){
        return html`
            ${this.forma.map(character => html`
                <div class="card">
                    <table class= "card-content">
                        <thead>
                            <tr>
                        <th>${character.id}</th>
                        <th>${character.employee_name}</th>
                        <th>${character.employee_salary}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            `)}
        `;
    }
  


}
customElements.define('ejemplo-1', Ejemplo1);